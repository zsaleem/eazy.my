## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - create-react-app
 - React
 - React Hooks
 - Redux
 - Redux Saga
 - JSX
 - CSS Grid
 - Styled-Components
 - Google Fonts
 - ES6+
 - Git
 - Gitflow
 - Gitlab
 - Gitlab CI/CD
 - JSDocs
 - Heroku
 - NPM/Yarn
 - Sublime Text
 - Font Awesome
 - Mac OS X
 - Google Chrome Incognito
 - Webpack
 - Babel
 - nodejs(16.4.0)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. Run `yarn install` command to download and install all dependencies.
    3. To run this project use `yarn start-client` command in command line.
    4. To build the project for production run `yarn build` command.
    5. To run production ready version on local environment, run `yarn start`. Then go to `http://localhost:8080`.

## Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

In this task I used `React 17.0.2`. I made use of `React Hooks`, `React Redux` and `React Router DOM` for routing. All the components for this project resides in `/src/components/` folder. Every component has its own folder with its file name and `redux` and `sagas` folders. `Redux` has all the files associated with redux state management tool. `sagas` folder has the saga file for that specific component. `components/common` folder has all the components that are common to the entire application for reusability. I put constants inside `/src/constants/` folder which includes constant variables for `redux actions`. I implemented routing in `container/App.js` file.

I used `Gitflow` workflow where I have two git main git branches i.e. `master` and `develop`. I worked on feature branches created from `develop` branch and merged back with develop branch upon completion. For staging release, I push changes to develop branch. For production release, I create a new `release` branch with a version from develop branch and then merge that `release` branch with both `develop` and `master` branches. Finally, push all the features to `master` branch which deploys to production environment.

I used `gitlab CI/CD`. As I mentioned above I have two branches `master` and `develop`. So I setup `CI/CD` so that when any changes that are pushed to develop branch will automatically by deployed to staging environment and when I push changes to `master` branch then those changes are automatically deployed to production environment.

I created two environments on heroku i.e. `production` and `staging`. To view this porject in action on production environment then please [click here](https://eazy-production.herokuapp.com/).

## Notes
The `breadcrumb` on view page is completely static. As I was not getting any information regarding breadcrumb from API calls so I simply added it as static component to fill the design requirements mentioned in XD mockups. Moreover, seller info avatar does not return and avatar in API for that reason I placed an img tag there. If API returns avatar then it will be shown otherwise it won't be shown.

When user redirects from `listing` component to `view` component, the product on `view` component is taken from `redux store`. Information present in `redux store` has all the details including the image of the product. However, when user is redirected to view component with URL or by refreshing the browser, I get the product details from API call. With API call, it does not return an image of the product so the image will be missing. This was the requirement of this coding task.
