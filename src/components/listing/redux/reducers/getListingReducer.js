import * as types from '../../../../constants/';

/** 
 * Initial State
 * 
 * @type {State} 
 */
const initialState = {
	loading: false,
	error: false,
};

/**
 * Listing Reducer.
 * @redux
 * @type {Redux.Reducer} 
 * @param {Redux.Store} state - The current redux state
 * @param {Redux.Action} action - A redux action
 * @return {Redux.Store} The updated redux state
 */
export default function(state = initialState, action) {
	switch(action.type) {
		case types.GET_LISTING_REQUESTED:
			return {
				loading: true,
				error: false,
			}
		case types.GET_LISTING_SUCCESS:
			return {
				loading: false,
				error: false,
				data: action.response.data,
			}
		case types.GET_LISTING_ERROR:
			return {
				loading: false,
				error: true,
			}
		default:
			return state;
	}
}
