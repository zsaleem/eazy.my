import * as types from '../../../../constants/';

/**
 * Triggers Product Listing HTTP request to fetch data for all products 
 * for listing component.
 * 
 * @redux
 * @type {Redux.ActionCreator} 
 * @return {Object} A new object that contains the type of action.
 */
export const getListing = () => {
  return {
    type: types.GET_LISTING_REQUESTED,
  }
};

/**
 * Gets called when API request is successful.
 *
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} response - it contains array of all products.
 * @return {Object} A new object that contains the type of action and response.
 */
export const getListingSuccess = response => {
  return {
    type: types.GET_LISTING_SUCCESS,
    response,
  }
};

/**
 * Gets called when API request is failed.
 *
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} error - it contains error.
 * @return {Object} A new object that contains the type of action and error.
 */
export const getListingError = (error) => {
  return {
    type: types.GET_LISTING_ERROR,
    error,
  }
};
