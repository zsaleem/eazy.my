import { put, call } from 'redux-saga/effects';
import {
  getListingSuccess,
  getListingError,
} from '../redux/actions/listingActions';

/**
 * When requested calls service to get product items and dispatches action when finished.
 */
export function* getListingSaga() {
  try {
    const data = yield call(getListing);
    yield put(getListingSuccess(data));
  } catch(error) {
    yield put(getListingError(error));
  }
}

/**
 * Makes API call using fetch() method.
 * @return {Promise} It returns promise object with products list array.
 */
const getListing = async () => {
  const listings = await fetch(`https://5b35ede16005b00014c5dc86.mockapi.io/list`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  });

  if (!listings.ok) {
    throw new Error(`An error has occured ${listings.status}`);
  }

  const json = await listings.json();
  return json;
};