import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Title from '../common/title/title';
import Product from '../common/product/product';
import { getListing } from './redux/actions/listingActions';

import styled from 'styled-components';

// Main wrapper of listing component. I am setting its position
// moving alittle down with top property, I am setting its maximum
// width and font family.
export const Wrapper = styled.div`
	position: relative;
	top: 80px;
	max-width: 1366px;
	font-family: Roboto;
`;

// Container is the sub wrapper with boz-size property and its own
// width, margin and padding with background white color. On responsive
// I am resetting its padding.
export const Container = styled.div`
	box-sizing: border-box;
	max-width: 1196px;
	margin: 10px auto 0;
  padding: 40px 100px;
	background-color: #ffffff;
	@media (max-width: 768px) {
  	padding: 40px 14px;
	}
`;

// This wrappers sets the grid for the component. For desktop, there are
// 6 columns, for screen size 1080 there are 5 columns, for screensize 770
// there are 4 columns, for screens size 640 there are 3 columns and for 
// screensize 470 there are 2 columns.
const ProductWrapper = styled.div`
	display: grid;
	grid-template-columns: repeat(6, minmax(140px, 1fr));
	grid-gap: 10px;
	@media (max-width: 1080px) {
  	grid-template-columns: repeat(5, minmax(140px, 1fr)) !important;
	}
	@media (max-width: 770px) {
  	grid-template-columns: repeat(4, minmax(140px, 1fr)) !important;
	}
	@media (max-width: 620px) {
  	grid-template-columns: repeat(3, minmax(140px, 1fr)) !important;
	}
	@media (max-width: 470px) {
  	grid-template-columns: repeat(2, minmax(140px, 1fr)) !important;
  	padding: 0 7%;
	}
`;

/**
* Listing component that fetches all products from remote API call
* and rendering those items on the screen.
* @returns {JSX} Component User Interface.
*/
const Listing = () => {
	/**
	 * Call dispatch hook.
	 * 
	 * @type {function}
	 */
	const dispatch = useDispatch();
	/**
	 * Extract error, loading and data properties from listings global state.
	 * @type {object}
	 * @property {Boolean} error - true/false.
	 * @property {Boolean} loading - true/false.
	 * @property {Array} data - List of all products.
	 */
	const { error, loading, data = [] } = useSelector(state => state.listings);

	/**
	 * This runs on first render of Listing component.
	 * 
	 * @type {function}
	 */
	useEffect(() => {
		dispatch(getListing());
	}, []);

	return (
		<Wrapper> {/* Main wrapper component that wraps all components */}
			<Container> {/* To set different width and background color */}
				<Title text='Listing' /> {/* Title of the listing component */}
				{
					loading /* check if it is true then API call is happening */
					?
					<p>Loading...</p> /* show loading indicator if API call is happening */
					:
						error /* check if it is true then API call returned errors */
					 	?
					 	<p>Something went wrong</p> /* Show error message */
					 	:
					 	<ProductWrapper> {/* Wrapper to set the grid for the products for different screen sizes */}
					 	{
						 	data.map((product) => (
						 		/* Render product component with product details */
						 		<Product product={product} key={Math.random()} />
						 	))
					 	}
					 	</ProductWrapper>
				}
			</Container>
		</Wrapper>
	);
}

export default Listing;
