import { useState, useEffect, } from 'react';
import { useSelector, useDispatch, } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { Wrapper, Container, } from '../listing/listing';
import Breadcrumb from '../common/breadcrumb/breadcrumb';
import { getProductRequest } from './redux/actions/viewActions';
import ViewProduct from './viewProduct/viewProduct';

import styled from 'styled-components';

// Main wrapper of View component.
// Overrides Wrapper and Container
// styles.
const ViewWrapper = styled.div`
	${Wrapper} {
		font-family: 'Open Sans';
	}

	${Container} {
		@media (max-width: 768px) {
			padding: 0;
		}
	}
`;

/**
* View Component that if comming from listing page, retreives
* product item from redux store otherwise retreive from API
* call.
* @returns {JSX} Component User Interface.
*/
const View = () => {
	/**
	 * Get 'pathname' from browser url.
	 * @type {object}
	 */
	const { pathname } = useLocation();
	/**
	 * Extract id number from 'pathname'
	 * @type {string}
	 */
	const id = pathname.match(/\d+/g)[0];
	/**
	 * Extract data, loading and error properties from listings state.
	 * @type {object}
	 * @property {Array} data - an ID.
	 * @property {Boolean} loading - true/false.
	 * @property {Boolean} error - true/false
	 */
	const { data = [], loading, error } = useSelector(state => state.listings);
	
	/**
	 * Extract result from view state. This happens if data in store
	 * is not available.
	 * @type {object}
	 */
	const result = useSelector(state => state.view);
	/**
	 * Declare 'product' state variable. This is filled with data from listing
	 * state.
	 * @type {object, function}
	 */
	const [product, setProduct] = useState();
	/**
	 * Declare viewProduct state variable. This is filled with data from
	 * view state property.
	 * @type {object, function}
	 */
	const [viewProduct, setViewProduct] = useState();
	// 
	/**
	 * This counter updates View component with new data from server or from
	 * listings property of state.
	 * @type {number, function}
	 */
	const [updateCounter, setUpdateCounter] = useState(0);
	/**
	 * Call dispatch hook.
	 * @type {function}
	 */
	const dispatch = useDispatch();
	
	/**
	 * This runs on first render of View component and is called whenever
	 * updateCounter is updated.
	 * @type {function}
	 */
	useEffect(() => {
		// Check if data from listings state property is available.
		// In other words, user is comming from listing component/page.
		if (data.length > 0) {
			// If there are items in data iterate through and find the
			// current product.
			data.map(product => {
				// Find the current product.
				if (product.id === id) {
					// Set the product object to product state variable.
					setProduct(product);
				}
			});
		} else {
			// else the user is comming to this page with direct link.
			// dispatch event to get data from API call.
			dispatch(getProductRequest(id));
		}
	}, [updateCounter]);
	// This is called when result is updated/changed.
	useEffect(() => {
		// Once there is updated result data, set it to viewProduct state
		// variable.
		setViewProduct(result.viewProduct);
	}, [result]);

	/**
	* updates counter to force View component to rerender and get updated
	* data either from listings state property or from API call. This is
	* necessary when user clicks on 'Similar Item' product then we need
	* to update view component because it is already rendered.
	*/
	const updateViewComponent = () => {
		setUpdateCounter(() => updateCounter + 1);
	};

	return (
		<ViewWrapper>
			<Wrapper>
				<Container>
					{/*
						Add a breadcrumb. This is static breadcrumb because the API
						does not return any properties associated to breadcrumb.
						Therefore, to meet XD design requirement, I implemented it
						as static component.
					*/}
					<Breadcrumb />
					{
						/* Check if loading and result loading it true */
						loading || result.loading
						?
						/* If they are true show Loading... indicator */
						<p>Loading...</p>
						:
							/* If error and result error are true */
							error || result.error
							?
							/* show error because there is an error */
							<p>Something went wrong</p>
							:
								/* check if product has attributes property */
								product?.hasOwnProperty('attributes')
								?
								/*
									If it has then render ViewProduct component with product
									data from listing state property.
								*/
								<ViewProduct product={product} updateViewComponent={updateViewComponent} />
								:
								/*
									If it does not have then render ViewProduct component with product
									data from view state property.
								*/
								<ViewProduct product={viewProduct} updateViewComponent={updateViewComponent} />
					}
				</Container>
			</Wrapper>
		</ViewWrapper>
	);
}

export default View;
