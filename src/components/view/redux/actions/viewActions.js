import * as types from '../../../../constants/';

/**
 * Triggers Get Product HTTP request to fetch data for View component.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} payload - Basically contains id of the product.
 * @return {Object} A new object that contains the type of action and payload.
 */
export const getProductRequest = (payload) => {
  return {
    type: types.GET_PRODUCT_REQUESTED,
    payload,
  }
};

/**
 * Gets Triggered when the API call for getting product data is successful.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} response - Contains response data received from the server.
 * @return {Object} A new object that contains the type of action and response.
 */
export const getProductSuccess = response => {
  return {
    type: types.GET_PRODUCT_SUCCESS,
    response,
  }
};

/**
 * Gets Triggered when the API call for getting product data is failed.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} error - Contains error detials.
 * @return {Object} A new object that contains the type of action and error.
 */
export const getProductError = (error) => {
  return {
    type: types.GET_PRODUCT_ERROR,
    error,
  }
};
