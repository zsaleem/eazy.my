import * as types from '../../../../constants/';

/** 
 * Initial State
 * 
 * @type {State} 
 */
const initialState = {
	loading: false,
	error: false,
};

/**
 * View Reducer.
 * @redux
 * @type {Redux.Reducer} 
 * @param {Redux.Store} state - The current redux state
 * @param {Redux.Action} action - A redux action
 * @return {Redux.Store} The updated redux state
 */
export default function(state = initialState, action) {
	switch(action.type) {
		case types.GET_PRODUCT_REQUESTED:
			return {
				loading: true,
				error: false,
			}
		case types.GET_PRODUCT_SUCCESS:
			return {
				loading: false,
				error: false,
				viewProduct: action.response.data,
			}
		case types.GET_PRODUCT_ERROR:
			return {
				loading: false,
				error: true,
			}
		default:
			return state;
	}
}
