import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faPhone,
	faEnvelope,
	faCommentAlt,
} from '@fortawesome/free-solid-svg-icons';

import styled from 'styled-components';

// It is the second row in ProductDetail container. It contains
// contact related items. I am setting its width to 325px according
// requirements for this coding task. I am moving it to the top by
// -20px and giving some padding. I am aligning text to the center
// with border top. For responsiveness. I am setting it fixed position
// so that it is fixed to the bottom of the screen for small screens.
// Links/a are the buttons and they are displayed next to each other.
const ContactContainer = styled.div`
	width: 325px;
	margin-top: -20px;
	padding: 15px 30px;
	text-align: center;
	border-top: 1px solid #F0F0EE;
	@media (max-width: 768px) {
		position: fixed;
  	width: 100%;
  	bottom: 0;
    left: 0;
    padding: 10px 0;
  	box-sizing: border-box;
    background: #FFFFFF;
    z-index: 1;
    box-shadow: 0 -3px 6px #00000029;

    a {
    	display: inline-block !important;
    	margin-right: 10px !important;
    	padding: 10px;
    }
	}
`;

// This is the title of the ContactDetails container. I am setting
// some fonts styles and hiding it for small screen sizes.
const ContactDetailsTitle = styled.span`
	font-size: 12px;
	font-weight: 400;
	color: #333333;
	@media (max-width: 768px) {
		display: none;
	}
`;

// Contact buttons container which includes common styles for all
// the buttons. First I am setting it displya block with padding
// and font styles. I am setting its border and radius. Then I am
// setting its label's properties for fonts and overrinding some
// margin left and font size properties for smaller screen size.
const ButtonsContainer = styled.div`
	a {
		display: block;
		margin-bottom: 10px !important;
		padding: 10px 15px;
		font-weight: 600;
		text-align: left;
		text-decoration: none;
		border: 1px solid #E01A1A;
		border-radius: 5px;
		span {
			margin-left: 20px;
			font-size: 16px;
		}
		@media (max-width: 768px) {
			span {
				margin-left: 5px;
				font-size: 14px;
			}
		}
	}
`;

// This is the phone button with its specific styles.
// I am setting its margin top with red color
// and white background.
const BtnPhone = styled(Link)`
	margin-top: 10px;
	color: #E01A1A;
	background-color: #ffffff;
`;

// Email button resetting all its margins first then overriding 
// it with margin top and color red and background white.
const BtnEmail = styled(Link)`
	margin: 0 !important;
	margin-top: 10px;
	color: #E01A1A;
	background-color: #ffffff;
`;

// Chat button with white color font and red background.
const BtnChat = styled(Link)`
	color: #FFFFFF;
	background-color: #E01A1A;
`;

const Contact = ({ contact }) => (
	<ContactContainer> {/* Second row, in second column, it contains contact actions */}
		{/* Title for contact section */}
		<ContactDetailsTitle>Interested with the ad? Contact the seller</ContactDetailsTitle>
		<ButtonsContainer> {/* Buttons container that contains contact action buttons */}
			<BtnPhone to='#'> {/* Phone button */}
				<FontAwesomeIcon icon={faPhone} /> {/* Phone button's font awesome icon */}
				<span> {/* Phone button's label */}
					{contact?.phone}
				</span>
			</BtnPhone>
			<BtnEmail to='#'>{/* Email button */}
				<FontAwesomeIcon icon={faEnvelope} /> {/* Email button's font awesome icon */}
				<span>Email</span> {/* Email button's label */}
			</BtnEmail>
			<BtnChat to='#'> {/* Chat button */}
				<FontAwesomeIcon icon={faCommentAlt} /> {/* Chat button's font awesome icon */}
				<span>Chat</span> {/* Chat button's label */}
			</BtnChat>
		</ButtonsContainer>
	</ContactContainer>
);

export default Contact;
