import SimilarItems from '../../similar/similar';
import Info from './info/info';
import Contact from './contact/contact';
import Description from './description/description';

import styled from 'styled-components';

// Styles for the title component. This is the title
// for product.
const Title = styled.h2`
	display: grid;
  grid-template-columns: 2fr 1fr;
  grid-gap: 20px;
  padding-right: 94px;
	font-size: 20px;
	color: #333333;
	font-weight: 600;
	@media (max-width: 768px) {
		padding: 15px 10px 0;
		grid-template-columns: 1fr;
	}
`;

// Main container of the product view page. I am setting its
// display to grid and giving it 2 columns with gap 20px. 
// For responsiveness convert to single column and 2 rows.
const ViewContainer = styled.div`
	display: grid;
	grid-gap: 20px;
	grid-template-columns: 2fr 1fr;
	@media (max-width: 768px) {
		grid-template-columns: 1fr;
		grid-template-rows: 2fr 1fr;
	}
`;

// Product Image container
const ProductImage = styled.div``;

// It contains the main image of the product. I am setting
// its box sizing, position and making it flex display to align
// items in the center. I am setting it height and width with
// border property. In image container, there is img itself.
// I am removing all margins from it, making it absolute with top
// and left position and tranforming it to the dead center of the 
// ImageContainer.
const ImageContainer = styled.figure`
	box-sizing: border-box;
	position: relative;
	margin: 0;
	display: flex;
  justify-content: center;
  height: 100%;
  width: 100%;
	border: 1px solid #B9B9B9;

	img {
		margin: 0;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	}
`;

// This section is the second column on first row next to
// ProductImage container. Here I am setting its border.
// It has 2 links which are wishlist and share and I am
// setting its font and aligning properties. For responsiveness
// I am resetting its margin top, removing border, adding
// bottom border.
const ProductDetails = styled.div`
	border: 1px solid #F0F0EE;
	.wishlist, .share {
		font-size: 600;
    margin-left: -1px;
	}
	.share {
		margin-left: 10px;
	}
	.wishlist {
		margin-right: 40px;
	}
	@media (max-width: 768px) {
		margin-top: 0;
		border: 0 solid #F0F0EE;
		border-bottom: 1px solid #F0F0EE;
	}
`;

// Container that contains similar items. I am setting its
// top border and for small screen sizes, I am setting it
// minimum height to 800px and padding left, right 10px.
// The I am algning everything inside this container to
// the center.
const SimilarItemsContainer = styled.div`
	border-top: 1px solid #F0F0EE;
	@media (max-width: 768px) {
		min-height: 800px;
		padding: 0 10px;
		text-align: center;
	}
`;

// This is the title of similar items container. I am setting
// font family, size, weight, color and aligning it to the
// left. For small screens set a padding ot 10px;
const SimilarItemsTitle = styled.h2`
	font-family: 'Open Sans';
	font-size: 16px;
	font-weight: 600;
	color: #707070;
	text-align: left;
	@media (max-width: 768px) {
		padding: 10px;
	}
`;

/**
* View Product Component which basically gets products object from
* parent's component. It renders all details about the current
* product.
* @param {object} Product details we want to display on the page.
* @param {function} Function to update counter which triggers refetching
* of product data either from API call or from global state in parent
* component.
* @returns {JSX} Component User Interface.
*/
const ViewProduct = ({ product, updateViewComponent }) => {

	/**
	* This calls updateViewComponent method in parent's component.
	*/
	const updateViewProduct = () => {
		updateViewComponent();
	};

	return (
		<>
			{/* Title component to display title of the product */}
			<Title>{product?.attributes?.title}</Title>
			<ViewContainer> {/* Main container for View component with grid property */}
				<ProductImage> {/* First row, first column that contains image of product */}
					<ImageContainer> {/* Image figure that carries image */}
						{/* Image of the product */}
						<img
							src={process.env.PUBLIC_URL + `${product?.attributes?.links?.image}`}
							alt={product?.attributes?.title}
						/>
					</ImageContainer>
				</ProductImage>
				{/* First row, second column, it contains all product details */}
				<ProductDetails>
					<Info product={product?.attributes} />
					<Contact contact={product?.attributes} />
				</ProductDetails>
				<Description description={product?.attributes?.description} />
			</ViewContainer>
			<SimilarItemsContainer> {/* Similar items section */}
				<SimilarItemsTitle>SIMILAR ITEMS</SimilarItemsTitle> {/* Similar items title */}
				<SimilarItems updateViewProduct={updateViewProduct} /> {/* Similar items component */}
			</SimilarItemsContainer>
		</>
	)
};

export default ViewProduct;
