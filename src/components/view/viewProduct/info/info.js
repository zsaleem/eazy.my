import Wishlist from '../../../common/wishlist/wishlist';
import Share from '../../../common/share/share';
import { ProductPrice } from '../../../common/product/product';

import styled from 'styled-components';

// This is the first row in ProductDetails container.
// I am setting its padding, and overriding margin top
// position and for responsiveness I am setting its
// padding.
const InfoContainer = styled.div`
	padding: 10px 30px 35px;
	${ProductPrice} {
		margin-top: 0;
	}
	@media (max-width: 768px) {
		padding: 10px;
	}
`;

// This is the titles inside Contact container which is 
// first row in ProductDetails container. I am setting its
// fonts styles with bottom margin zero.
const ProductDetailsLabel = styled.h5`
	margin-bottom: 0;
	font-size: 12px;
	font-weight: 400;
	color: #707070;
`;

// This contains values for the ProductDetailsLabel
// I am resetting it padding and margins and giving some
// font related styles.
const TextValue = styled.p`
	padding: 0;
	margin: 0;
	font-weight: 400;
	font-size: 14px;
	color: #333333;
`;

// It contains seller's details
const SellerValue = styled.div``;

// It is the avatar of the seller. I am setting its display
// so that rest of the contents appear next to avatar.
// Giving width and height and background-color.
const Avatar = styled.img`
	display: inline-block;
	width: 48px;
	height: 48px;
	background-color: #eeeeee;
`;

// it contains seller's name and type. I am giving some gap
// on the left side and displaying next to avatar with vertical
// align to top.
const SellerItems = styled.div`
	margin-left: 5px;
	display: inline-block;
  vertical-align: top;
`;

// I am setting font related styles to seller type.
const SellerType = styled.span`
	font-weight: 400;
	color: #707070;
	font-size: 12px;
`;

const Info = ({ product }) => (
	<InfoContainer> {/* first row, it contains product price, condition, location etc */}
		<Wishlist cssClass='wishlist' /> {/* Wishlist component to save products to wishlist */}
		<Share cssClass='share' /> {/* To share product */}
		<ProductDetailsLabel>Price</ProductDetailsLabel> {/* Product price title */}
		<ProductPrice>{product?.price}</ProductPrice> {/* Product price value */}
		<ProductDetailsLabel>Item condition</ProductDetailsLabel> {/* Condition title */}
		<TextValue>{product?.condition}</TextValue> {/* Condition value */}
		<ProductDetailsLabel>Item location</ProductDetailsLabel> {/* Product location */}
		<TextValue>{product?.location}</TextValue> {/* Product location value */}
		<ProductDetailsLabel>Seller info</ProductDetailsLabel> {/* Product owner info */}
		<SellerValue> {/* product owner's container */}
			<Avatar src='' alt='' /> {/* Owner's Avatar */}
			<SellerItems> {/* Owner's details container */}
				<TextValue>{product?.seller_name}</TextValue> {/* Owner's name */}
				<SellerType>{product?.seller_type}</SellerType> {/* Owner's type */}
			</SellerItems>
		</SellerValue>
	</InfoContainer>
);

export default Info;
