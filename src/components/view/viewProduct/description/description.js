import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFlag } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

// Description container that contains the entire description
// of the product in several paragraphs. This section takes 
// 2 columns space. It has a Link tag for Report Ad which is
// specific styles. Link tag has a span tag with Link label
// where I am setting its margin-left so that there is a gap 
// between icon and label. For responsiveness, set description
// container's padding and Link's right position so that there
// is a gap on the right side of the link.
const ProductDescription = styled.div`
	position: relative;
	grid-column-start: 1;
  grid-column-end: 2;
  a {
  	font-size: 14px;
    color: #707070;
    text-decoration: none;
    position: absolute;
    right: 0;
    top: 22px;

    span {
    	margin-left: 10px;
    }
  }
  @media (max-width: 768px) {
  	padding: 0 10px;
  	a {
  		right: 10px;
  	}
  }
`;

// This is the title of the description section.
// I am setting its font size, weight and color
// from XD design mockups.
const DescriptionTitle = styled.h4`
	font-size: 16px;
	font-weight: 600;
	color: #E01A1A;
`;

// It is the paragraph where the descriptio of the 
// product shows up. I am setting certain font related
// styles.
const Desc = styled.p`
	font-weight: 400;
	font-size: 12px;
	color: #333333;
	white-space: pre-line;
`;

const Description = ({ description }) => (
	<ProductDescription> {/* Second row of main grid which contains product description */}
		<DescriptionTitle>Description</DescriptionTitle> {/* Title of description section */}
		<Link to='#'> {/* Report Ad Link */}
			<FontAwesomeIcon icon={faFlag} /> {/* Report Ad icon */}
			<span>Report Ad</span> {/* Report Ad link's label */}
		</Link>
		{
			/*
				First split the description with newline delimiter which
				gives an array of separate paragraphs, then iterate over
				that array and display each and every paragraph from the
				description.
			*/
			description?.split('\\n').map(text => (
				<Desc key={Math.random()}>{text}</Desc>
			))
		}
	</ProductDescription>
);

export default Description;
