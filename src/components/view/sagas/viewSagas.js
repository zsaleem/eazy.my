import { put, call } from 'redux-saga/effects';
import {
  getProductSuccess,
  getProductError,
} from '../redux/actions/viewActions';

/**
 * When requested calls service to get product item and dispatches action when finished.
 * @param {object} payload - Contains id of the product that needs to fetched from the server.
 */
export function* getProductSaga(payload) {
  try {
    const data = yield call(getProduct, payload.payload);
    yield put(getProductSuccess(data));
  } catch(error) {
    yield put(getProductError(error));
  }
}

/**
 * Makes API call using fetch() method to fetch product's details.
 * @param {number} id - id of current product.
 * @return {Promise} It returns promise object with product's object.
 */
const getProduct = async (id) => {
  const listings = await fetch(`https://5b35ede16005b00014c5dc86.mockapi.io/view/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  });

  if (!listings.ok) {
    throw new Error(`An error has occured ${listings.status}`);
  }

  const json = await listings.json();
  return json;
};