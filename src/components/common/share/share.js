import { Wrapper } from '../wishlist/wishlist';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareAlt } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

/**
* Share component which is used in view component to share the current
* product.
* 
* @param {string} cssClass - It is a css class to apply on the wrapper.
* @returns {JSX} Component User Interface.
*/
const Share = ({ cssClass }) => {
	return (
		<Wrapper to='#' className={cssClass}>
			<FontAwesomeIcon icon={faShareAlt} />
			<span>Share</span>
		</Wrapper>
	);
}

export default Share;
