import styled from 'styled-components';
import { Link } from 'react-router-dom';

// Main header wrapper to set global styles for this component.
// I am making it fixed position, with margin top and width 100%,
// height sets to 80px and I am setting font styles with background
// white and z-index 1.
const HeaderWrapper = styled.div`
	box-sizing: border-box;
	position: fixed;
	margin-top: -17px;
	width: 100%;
	height: 80px;
	font-family: Open Sans;
	font-weight: 600;
	background-color: #ffffff;
	box-shadow: 0 3px 6px #00000029;
	z-index: 1;
`;

// Sub container with more specific styles. I am setting this to fixed
// with max width property and height same as HeaderWrapper with margin
// left. I making it a grid with 2 columns and background color white.
// for mobile devices I remove margins and reset columns width.
const HeaderContainer = styled.div`
	position: fixed;
	max-width: 1196px;
	height: 80px;
	margin: 0 0 0 60px;
	display: grid;
  grid-template-columns: .5fr auto;
  grid-gap: 100px;
  background-color: #ffffff;
  @media (max-width: 880px) {
  	margin: 0;
  	grid-template-columns: .5fr 3fr;
	}
`;

// Logo of the coding task. I am setting margins to it. With red color
// with bold font and no text decoration. On mobile devices I set
// margin left.
const Logo = styled(Link)`
	margin-top: 5px;
  margin-left: 36px;
	color: #D90E0E;
	font-weight: bold;
	text-decoration: none;
	@media (max-width: 880px) {
  	margin-left: 12px;
	}
`;

// It sets the size of the logo. Logo contains 2 parts. One is small
// and one is big. This component is for big size part.
const Large = styled.span`
	font-size: 50px;
`;

// It sets the size of the logo. Logo contains 2 parts. One is small
// and one is big. This component is for small size part.
const Small = styled.span`
	font-size: 30px;
`;

// This is for desktop naviation container. I am setting padding with
// no margins and I hide it on small screen sizes.
const DesktopNavigation = styled.ul`
	padding: 35px 0 0 0;
	margin: 0;
	@media (max-width: 880px) {
  	display: none;
	}
`;

// It is the container that contains label of the navigation item.
const ListItem = styled.li`
	display: inline-block;
	margin-right: 50px;
	list-style: none;
	@media (max-width: 798px) {
  	margin-right: 20px;
	}
`;

// It is a link inside ListItem with its own font styles.
const Item = styled(Link)`
	color: #333333;
	text-decoration: none;
	font-weight: Semibold;
`;

// Mobile navigation container. It is absolutely positioned.
// With top and right properties. On large screens it is hidden
// and on small screens it is displayed.
const MobileNavigation = styled(Link)`
	top: 36px;
  position: absolute;
  right: 12px;
	@media only screen and (min-width: 880px) {
  	display: none;
	}
	@media (max-width: 880px) {
  	display: block;
	}
`;

// Hamburger menu with its own properties and styles.
const Hamburger = styled.span`
	display: block;
	width: 30px;
	height: 1px;
	margin-bottom: 2px;
	background-color: #333333;
	border: 1px solid #333333;
`;

/**
 * Header component that renders the main header of this coding task.
 * 
 * @returns {JSX} Component User Interface.
 */
const Header = () => (
	<HeaderWrapper>
		<HeaderContainer>
			<Logo to='/'>
				<Large>eazy</Large>
				<Small>.my</Small>
			</Logo>
			<DesktopNavigation>
				<ListItem>
					<Item to=''>Categories</Item>
				</ListItem>
				<ListItem>
					<Item to=''>Notification</Item>
				</ListItem>
				<ListItem>
					<Item to=''>Login /Sign up</Item>
				</ListItem>
				<ListItem>
					<Item to=''>Help</Item>
				</ListItem>
			</DesktopNavigation>
			<MobileNavigation to=''>
				<Hamburger></Hamburger>
				<Hamburger></Hamburger>
				<Hamburger></Hamburger>
			</MobileNavigation>
		</HeaderContainer>
	</HeaderWrapper>
);

export default Header;
