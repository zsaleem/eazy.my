import styled from 'styled-components';

// Main wrapper to set some global styles for title component.
const TitleWrapper = styled.h1`
	font-family: Roboto;
	font-weight: Medium,
	font-size: 18px;
	color: #333333;
	grid-column-start: 1;
  grid-column-end: 7;
  text-transform: uppercase;
`;

/**
* Title component to render the title of the page/component
* 
* @param {string} text - It is a label of the title.
* @returns {JSX} Component User Interface.
*/
const Title = ({ text }) => {
	return (
		<TitleWrapper>{text}</TitleWrapper>
	);
}

export default Title;
