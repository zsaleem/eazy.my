import { Link } from 'react-router-dom';
import styled from 'styled-components';

// Main product container to set global styles for this component.
// I am setting it as block container with width 140px with no
// text decoration and border 1px.
const ProductContainer = styled(Link)`
	display: block;
	width: 140px;
	text-decoration: none;
	border: 1px solid #D8D8D8;
`;

// I am overriding default padding and margins for figure tag.
const ImageFigure = styled.figure`
	margin: 0;
  padding: 0;
`;

// I am setting image's tag width to 100%.
const ProductImage = styled.img`
	width: 100%;
`;

// I am giving padding to figcaption.
const Caption = styled.figcaption`
	padding: 10px;
`;

// Setting some font styles to the title of this product.
const ProductTitle = styled.span`
	font-weight: 500;
	font-size: 12px;
	color: #4D4D4D;
`;

// I am setting display block with top margin and font styles.
export const ProductPrice = styled.span`
	display: block;
	margin-top: 10px;
	font-weight: 700;
	font-size: 14px;
	color: #E01A1A;
`;

/**
* Product component that renders single product on the screen,
* 
* @param {object} product - It contains all the detail about the product.
* @returns {JSX} Component User Interface.
*/
const Product = ({ product }) => {
	return (
		<ProductContainer to={`${product.attributes.links.self}`}>
			<ImageFigure>
				<ProductImage
					src={process.env.PUBLIC_URL + `${product.attributes.links.image}`}
					alt={product.attributes.title}
				/>
				<Caption>
					<ProductTitle>{product.attributes.title}</ProductTitle>
					<ProductPrice>{product.attributes.price}</ProductPrice>
				</Caption>
			</ImageFigure>
		</ProductContainer>
	);
}

export default Product;
