import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components';

// Main wrapper for global styles for this component.
export const Wrapper = styled(Link)`
	color: #707070;
	text-decoration: none;
	span {
		font-family: 'Open Sans',
		font-weight: 400;
		font-size: 14px;
		margin-left: 10px;
	}

	.fa-heart {
		-webkit-text-fill-color: transparent;
	}
`;

/**
* Wishlist component that is used in view component to add the
* current product to wishlist.
* 
* @param {string} cssClass - It is css class to be applied on the wrapper.
* @returns {JSX} Component User Interface.
*/
const Wishlist = ({ cssClass }) => {
	return (
		<Wrapper to='#' className={cssClass}>
			<FontAwesomeIcon icon={farHeart} />
			<span>Wishlist</span>
		</Wrapper>
	);
}

export default Wishlist;
