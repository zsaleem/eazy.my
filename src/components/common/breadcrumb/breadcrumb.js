import { Link } from 'react-router-dom';
import styled from 'styled-components';

// Breadcrumb wrapper to set global styles for this component.
// I am setting font styles and on mobile screens it disappears.
const BreadcrumbWrapper = styled.div`
	font-family: 'Open Sans';
	font-weight: 400;
	font-size: 11px;
	@media (max-width: 880px) {
		display: none;	
	}
`;

// I am overriding link's text decoration and setting its color.
const LinkTo = styled(Link)`
	text-decoration: none;
	color: #333333;
`;

/**
 * Breadcrumb component that renders static breadcrumb at the top of
 * view/product details component/page.
 * 
 * @returns {JSX} Component User Interface.
 */
const Breadcrumb = () => (
	<BreadcrumbWrapper>
		<span>
			<LinkTo to=''>Home</LinkTo> > 
			<LinkTo to=''>Electronics</LinkTo> > 
			<LinkTo to=''>Games & Console</LinkTo> > 
			<LinkTo to=''>Nintendo Switch Neon Joy-Con (1 Year MaxSoft Warranty)+Screen Protector</LinkTo>
		</span>
	</BreadcrumbWrapper>
);

export default Breadcrumb;
