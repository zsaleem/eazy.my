import * as types from '../../../../constants/';

/**
 * Triggers SimilarItems HTTP request to fetch data for SimilarItems component.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Payload} payload - Basically contains id of current product.
 * @return {Object} A new object that contains the type of action and payload.
 */
export const getSimilarItemsRequest = (payload) => {
  return {
    type: types.GET_SIMILAR_ITEM_REQUESTED,
    payload,
  }
};

/**
 * Gets triggered when the API call is successful.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} response - It contains the response from the server.
 * @return {Object} A new object that contains the type of action and response.
 */
export const getSimilarItemsSuccess = response => {
  return {
    type: types.GET_SIMILAR_ITEM_SUCCESS,
    response,
  }
};

/**
 * Gets triggered when there is any error with API call.
 * @redux
 * @type {Redux.ActionCreator} 
 * @param {Object} error - It contains detailed information about the type of error occurred.
 * @return {Object} A new object that contains the type of action and error.
 */
export const getSimilarItemsError = (error) => {
  return {
    type: types.GET_SIMILAR_ITEM_ERROR,
    error,
  }
};
