import * as types from '../../../../constants/';

/** 
 * Initial State
 * 
 * @type {State} 
 */
const initialState = {
	loading: false,
	error: false,
};

/**
 * SimilarItems reducer.
 * @redux
 * @type {Redux.Reducer} 
 * @param {Redux.Store} state - The current redux state
 * @param {Redux.Action} action - A redux action
 * @return {Redux.Store} The updated redux state
 */
export default function(state = initialState, action) {
	switch(action.type) {
		case types.GET_SIMILAR_ITEM_REQUESTED:
			return {
				loading: true,
				error: false,
			}
		case types.GET_SIMILAR_ITEM_SUCCESS:
			return {
				loading: false,
				error: false,
				similarItems: action.response.data,
			}
		case types.GET_SIMILAR_ITEM_ERROR:
			return {
				loading: false,
				error: true,
			}
		default:
			return state;
	}
}
