import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, Link } from 'react-router-dom';
import { getSimilarItemsRequest } from './redux/actions/similarItemsActions';

import styled from 'styled-components';

// Main container contains all similar items/products.
// I am displaying them inline block so all items are
// aligned next to each other. I am setting some padding
// to it with width, resetting text decoration, setting 
// margins and border and border radius.
const SimilarItemsContainer = styled(Link)`
	display: inline-block;
	padding: 3px;
  width: 123px;
  text-decoration: none;
  margin-right: 13px;
  margin-bottom: 13px;
  border-radius: 2px;
  border: 1px solid #F0F0EE;
`;

// This contains the image of the product and I am 
// simply resetting its default padding and margins.
const ImageContainer = styled.figure`
  margin: 0;
  padding: 0;
`;

// The image itself, I am giving it width 100% and
// aligning them to the top of the container.
const ProductImage = styled.img`
	width: 100%;
	vertical-align: top;
`;

// The title of the product with margins, font related
// styles.
const Title = styled.p`
	margin: 3px;
	font-family: 'Open Sans';
	font-weight: 400;
	font-size: 10px;
	color: #707070;
`;

// The price component with its specific font styles.
const Price = styled.span`
	font-family: 'Open Sans';
	font-weight: 600;
	font-size: 13px;
	color: #E01A1A;
`;

/**
* Similar items component that fetches similar items from remote API call
* and rendering those items on the screen.
* @param {function} Function to update counter which triggers refetching
* of product data either from API call or from global state in parent
* component.
* @returns {JSX} Component User Interface.
*/
const SimilarItems = ({ updateViewProduct }) => {
	/**
	 * Get 'pathname' from browser url.
	 * @type {object}
	 */
	const { pathname } = useLocation();
	/**
	 * Extract id number from 'pathname'
	 * @type {string}
	 */
	const id = pathname.match(/\d+/g)[0];
	/**
	 * Call dispatch hook.
	 * @type {function}
	 */
	const dispatch = useDispatch();
	/**
	 * Extract similarItems, loading and error properties from items global state.
	 * @type {object}
	 * @property {Boolean} loading - true/false.
	 * @property {Boolean} error - true/false.
	 * @property {Array} similarItems - List of items similar to current product.
	 */
	const { loading, error, similarItems } = useSelector(state => state.items);

	/**
	 * This runs on first render of SimilarItems component.
	 * @type {function}
	 */
	useEffect(() => {
		// dispatch action to trigger similar items request.
		dispatch(getSimilarItemsRequest(id));
	}, []);

	/**
	 * This calls the updateViewProduct which is a method defined in parent
	 * component to trigger refetching of data from remote API or from global
	 * state. It also scrolls the viewport to the top of the page to show new
	 * product details.
	 */
	const updateProductView = () => {
		updateViewProduct();
		window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
	};

	return (
		loading /* check if loading is true */
		?
		<p>Loading...</p> /* then show loading indicator */
		:
			error /* if there are any errors then this will be true */
			?
			<p>Something went wrong</p> /* Show error message */
			:
				similarItems !== undefined /* check if similarItems are not undefined */
				?
					similarItems.map((item) => ( /* iterate through similarItems array */
						/* This is a Link which points to view component and trigger updateProductView method */
						<SimilarItemsContainer to={`${item.attributes?.href}`} key={Math.random()} onClick={updateProductView}>
							<ImageContainer> {/* figure tag */}
								<ProductImage /* image tag with product image */
									src={process.env.PUBLIC_URL + `${item.attributes?.image}`}
									alt={item.attributes?.title}
								/>
								<figcaption>
									<Title>{item.attributes.title}</Title> {/* Product title */}
									<Price>{item.attributes.price}</Price> {/* Product price */}
								</figcaption>
							</ImageContainer>
						</SimilarItemsContainer>
					))
				:
				''
	);
}

export default SimilarItems;
