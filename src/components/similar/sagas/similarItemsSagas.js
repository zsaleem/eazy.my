import { put, call } from 'redux-saga/effects';
import {
  getSimilarItemsSuccess,
  getSimilarItemsError,
} from '../redux/actions/similarItemsActions';

/**
 * When requested calls service to get similar items and dispatches action when finished.
 * @param {object} payload - redux standard action/payload with current product id.
 */
export function* getSimilarItemsSaga(payload) {
  try {
    const data = yield call(getSimilarItems, payload.payload);
    yield put(getSimilarItemsSuccess(data));
  } catch(error) {
    yield put(getSimilarItemsError(error));
  }
}

/**
 * Makes API call using fetch() method.
 * @param {number} id - id of current product.
 * @return {Promise} It returns promise object with similar items array.
 */
const getSimilarItems = async (id) => {
  const listings = await fetch(`https://5b35ede16005b00014c5dc86.mockapi.io/similar/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  });
  if (!listings.ok) {
    throw new Error(`An error has occured ${listings.status}`);
  }
  const json = await listings.json();
  return json;
};