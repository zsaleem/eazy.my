import { useEffect } from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';

import Header from '../components/common/header/header';
import Listing from '../components/listing/listing';
import View from '../components/view/view';

const App = () => {
  useEffect(() => {
    document.title = 'Mudah - Coding Task';
  }, []);

  return (
    <BrowserRouter>
      <Header />
      <div>
        <Switch>
          <Route path='/' exact={true}><Listing /></Route>
          <Route path='/view'><View /></Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;