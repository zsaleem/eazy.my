import { takeLatest } from 'redux-saga/effects';
import { getListingSaga } from '../components/listing/sagas/listingSagas';
import { getProductSaga } from '../components/view/sagas/viewSagas';
import { getSimilarItemsSaga } from '../components/similar/sagas/similarItemsSagas';

import * as types from '../constants/';

export default function* watchUserAuthentication() {
	yield takeLatest(types.GET_LISTING_REQUESTED, getListingSaga);
	yield takeLatest(types.GET_PRODUCT_REQUESTED, getProductSaga);
	yield takeLatest(types.GET_SIMILAR_ITEM_REQUESTED, getSimilarItemsSaga);
}