import { combineReducers } from 'redux';
import listings from '../components/listing/redux/reducers/getListingReducer';
import view from '../components/view/redux/reducers/viewReducer';
import items from '../components/similar/redux/reducers/similarItemsReducer';

const rootReducer = combineReducers({
	listings, view, items,
});

export default rootReducer;